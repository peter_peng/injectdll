#include "stdafx.h"
#include <Dbghelp.h>

#pragma comment(lib,"Dbghelp.lib")

PIMAGE_SECTION_HEADER GetEnclosingSectionHeader(DWORD rva, PIMAGE_NT_HEADERS pNTHeader)
{
	PIMAGE_SECTION_HEADER section = IMAGE_FIRST_SECTION(pNTHeader);
	unsigned i;
	for ( i=0; i < pNTHeader->FileHeader.NumberOfSections; i++, section++ )
	{
		// This 3 line idiocy is because Watcom's linker actually sets the
		// Misc.VirtualSize field to 0.  (!!! - Retards....!!!)
		DWORD size = section->Misc.VirtualSize;
		if ( 0 == size )
			size = section->SizeOfRawData;

		// Is the RVA within this section?
		if ( (rva >= section->VirtualAddress) && 
			(rva < (section->VirtualAddress + size)))
			return section;
	}

	return 0;
}

LPVOID GetPtrFromRVA(PIMAGE_NT_HEADERS pNTHeader, PVOID imageBase, ULONG rva)
{
	PIMAGE_SECTION_HEADER pSectionHdr;
	INT delta;

	pSectionHdr = GetEnclosingSectionHeader( rva, pNTHeader );
	if ( !pSectionHdr )
		return 0;

	delta = (INT)(pSectionHdr->VirtualAddress-pSectionHdr->PointerToRawData);
	//return (LPVOID) ( (ULONG)imageBase + rva - delta );	
	return (LPVOID) ( (ULONG)imageBase + rva  );	
}

DWORD SetFunctionAddress(HMODULE hModule, LPCSTR szDllName, LPCSTR szFuncName, DWORD* lpOriginal, DWORD lpNewAddress)
{
	DWORD ret = ERROR_SUCCESS;
	if (hModule!=NULL && lpOriginal!=NULL && lpNewAddress!=NULL && szDllName && strlen(szDllName))
	{
		HMODULE h = hModule;
		if (h!=NULL)
		{
			PBYTE imageBase = (PBYTE)h;
			PIMAGE_DOS_HEADER dosHeader = (PIMAGE_DOS_HEADER)imageBase;
			PIMAGE_NT_HEADERS pNTHeader = ImageNtHeader(imageBase);//(PIMAGE_NT_HEADERS)((LPBYTE)dosHeader+dosHeader->e_lfanew);
			if (pNTHeader->Signature==0x4550)
			{
				BOOL done=FALSE;
				//PIMAGE_SECTION_HEADER sectionHeader=ImageRvaToSection(pNTHeader,imageBase,pNTHeader->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].VirtualAddress);
				PIMAGE_SECTION_HEADER sectionHeader=GetEnclosingSectionHeader(pNTHeader->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].VirtualAddress,pNTHeader);
				//PIMAGE_IMPORT_DESCRIPTOR pImportTable=(PIMAGE_IMPORT_DESCRIPTOR)ImageRvaToVa(pNTHeader,imageBase,pNTHeader->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].VirtualAddress,NULL);
				ULONG size=0;
				PIMAGE_IMPORT_DESCRIPTOR pImportTable=(PIMAGE_IMPORT_DESCRIPTOR)ImageDirectoryEntryToData(imageBase,TRUE,IMAGE_DIRECTORY_ENTRY_IMPORT,&size);
				DWORD flOldProtect=0;

				do 
				{
					if (!(pImportTable->TimeDateStamp==0 && pImportTable->Name==0))
					{
						char* dllName = (char *)GetPtrFromRVA(pNTHeader,imageBase,pImportTable->Name);
						logIt(_T("Library: %S\n"), dllName);
						if (_stricmp(dllName, szDllName) == 0)
						{
							PIMAGE_THUNK_DATA pINT = (PIMAGE_THUNK_DATA)GetPtrFromRVA(pNTHeader,imageBase,pImportTable->OriginalFirstThunk);
							PIMAGE_THUNK_DATA pIAT = (PIMAGE_THUNK_DATA)GetPtrFromRVA(pNTHeader,imageBase,pImportTable->FirstThunk);
							do 
							{
								if ( pINT->u1.AddressOfData != 0 )
								{
									if (IMAGE_SNAP_BY_ORDINAL32(pINT->u1.Ordinal))
									{
										// it is a ordinal
									}
									else
									{
										PIMAGE_IMPORT_BY_NAME pOrdinalName=(PIMAGE_IMPORT_BY_NAME)GetPtrFromRVA(pNTHeader,imageBase,pINT->u1.AddressOfData);
										//logIt("Function: %s\n", pOrdinalName->Name);
										if (strcmp((char*)pOrdinalName->Name,szFuncName)==0)
										{
											//if (!(sectionHeader->Characteristics&IMAGE_SCN_MEM_WRITE))
											{
												DWORD flOldProtect;
												//lpFuncAddress=(BOOL (WINAPI* )(HANDLE,LPCVOID,DWORD,LPDWORD,LPOVERLAPPED))pIAT->u1.AddressOfData;
												*lpOriginal=pIAT->u1.AddressOfData;
												if (VirtualProtect((LPVOID)&(pIAT->u1.AddressOfData),sizeof(DWORD),PAGE_READWRITE,&flOldProtect))
												{
													pIAT->u1.AddressOfData=lpNewAddress;
												}
											}


											done=TRUE;
										}
									}
									pIAT++;
									pINT++;
								}
								else break;
							} while(!done);
						}
						pImportTable++;
					}
					else break;
				} while(!done);
			}
		}
	}
	return ret;
}

DWORD SetFunctionAddressV2(HMODULE hModule, LPCSTR szDllName, short ordinal, DWORD* lpOriginal, DWORD lpNewAddress)
{
	DWORD ret = ERROR_SUCCESS;
	if (hModule!=NULL && lpOriginal!=NULL && ordinal>0 && szDllName && strlen(szDllName))
	{
		HMODULE h = hModule;
		if (h!=NULL)
		{
			PBYTE imageBase = (PBYTE)h;
			PIMAGE_DOS_HEADER dosHeader = (PIMAGE_DOS_HEADER)imageBase;
			PIMAGE_NT_HEADERS pNTHeader = ImageNtHeader(imageBase);//(PIMAGE_NT_HEADERS)((LPBYTE)dosHeader+dosHeader->e_lfanew);
			if (pNTHeader->Signature==0x4550)
			{
				BOOL done=FALSE;
				//PIMAGE_SECTION_HEADER sectionHeader=ImageRvaToSection(pNTHeader,imageBase,pNTHeader->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].VirtualAddress);
				PIMAGE_SECTION_HEADER sectionHeader=GetEnclosingSectionHeader(pNTHeader->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].VirtualAddress,pNTHeader);
				//PIMAGE_IMPORT_DESCRIPTOR pImportTable=(PIMAGE_IMPORT_DESCRIPTOR)ImageRvaToVa(pNTHeader,imageBase,pNTHeader->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].VirtualAddress,NULL);
				ULONG size=0;
				PIMAGE_IMPORT_DESCRIPTOR pImportTable=(PIMAGE_IMPORT_DESCRIPTOR)ImageDirectoryEntryToData(imageBase,TRUE,IMAGE_DIRECTORY_ENTRY_IMPORT,&size);
				DWORD flOldProtect=0;

				do 
				{
					if (!(pImportTable->TimeDateStamp==0 && pImportTable->Name==0))
					{
						char* dllName = (char*)GetPtrFromRVA(pNTHeader,imageBase,pImportTable->Name);
						logIt(_T("Library: %S\n"), dllName);
						if (_stricmp(dllName, szDllName) == 0)
						{
							PIMAGE_THUNK_DATA pINT = (PIMAGE_THUNK_DATA)GetPtrFromRVA(pNTHeader,imageBase,pImportTable->OriginalFirstThunk);
							PIMAGE_THUNK_DATA pIAT = (PIMAGE_THUNK_DATA)GetPtrFromRVA(pNTHeader,imageBase,pImportTable->FirstThunk);
							do 
							{
								if ( pINT->u1.AddressOfData != 0 )
								{
									if (IMAGE_SNAP_BY_ORDINAL32(pINT->u1.Ordinal))
									{
										// it is a ordinal
										short ord=LOWORD(pINT->u1.Ordinal);
										if(ord==ordinal)
										{
											DWORD flOldProtect;
											//lpFuncAddress=(BOOL (WINAPI* )(HANDLE,LPCVOID,DWORD,LPDWORD,LPOVERLAPPED))pIAT->u1.AddressOfData;
											*lpOriginal=pIAT->u1.AddressOfData;
											if (VirtualProtect((LPVOID)&(pIAT->u1.AddressOfData),sizeof(DWORD),PAGE_READWRITE,&flOldProtect))
											{
												pIAT->u1.AddressOfData=lpNewAddress;
											}
											done=TRUE;
										}
									}
									else
									{
									}
									pIAT++;
									pINT++;
								}
								else break;
							} while(!done);
						}
						pImportTable++;
					}
					else break;
				} while(!done);
			}
		}
	}
	return ret;
}

DWORD WriteMemory(LPVOID lpBaseAddress, LPCVOID lpBuffer, SIZE_T nSize)
{
	DWORD pid=GetCurrentProcessId();
	HANDLE h = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pid);
	if (h!=NULL)
	{
		WriteProcessMemory(h, lpBaseAddress,lpBuffer,nSize,NULL);
		CloseHandle(h);
	}
	return 0;
}
void SetJump(BOOL set, DWORD lpAddress, LPBYTE buffer, DWORD lpAddressJump)
{
	BYTE ready_write[5];
	BYTE* lpCode=(BYTE*)lpAddress;
	if (set)
	{
		VirtualProtect(lpCode,5,PAGE_EXECUTE_READWRITE,NULL);
		memcpy(buffer, lpCode, 5);
		DWORD offset=lpAddressJump - (DWORD)lpCode -5;
		ready_write[0]=0xe9;
		*(DWORD*)&ready_write[1]=offset;
	}
	else
	{
		VirtualProtect(lpCode,5,PAGE_EXECUTE_READWRITE,NULL);
		memcpy(ready_write,buffer,5);
	}
	//memcpy(lpCode, ready_write,5);
	WriteMemory(lpCode,ready_write,5);
}

TCHAR * get_apple_support_path(TCHAR* module)
{
	TCHAR* ret = NULL;
	if (module != NULL)
	{
		HKEY hKey;
		TCHAR buf[1024];
		_stprintf_s(buf, _T("SOFTWARE\\Apple Inc.\\%s"), module);
		if (RegOpenKeyEx(HKEY_LOCAL_MACHINE, buf, 0, KEY_READ, &hKey) == ERROR_SUCCESS)
		{
			DWORD size = MAX_PATH;
			ZeroMemory(buf, sizeof(buf));
			if (RegQueryValueEx(hKey, _T("InstallDir"), NULL, NULL, (LPBYTE)buf, &size) == ERROR_SUCCESS)
			{
				ret = _tcsdup(buf);
			}
			RegCloseKey(hKey);
		}
	}
	return ret;
}

void set_path()
{
	//char *p = _dupenv_s("PATH");
	TCHAR *p;
	size_t len;
	errno_t err = _tdupenv_s(&p, &len, _T("PATH"));
	if (err) return;
	//free(tmp);

	int nLen = _tcslen(p)*sizeof(TCHAR) + 2 * MAX_PATH;
	TCHAR *pp = (TCHAR*)malloc(nLen);
	if (pp != NULL)
	{
		_tcscpy_s(pp, nLen, _T("PATH="));
		_tcscpy_s(pp, nLen, p);
		//strcat(pp, ";C:\\Program Files (x86)\\Common Files\\Apple\\Apple Application Support;C:\\Program Files (x86)\\Common Files\\Apple\\Mobile Device Support");
		TCHAR* pAppleApplicationSupport = get_apple_support_path(_T("Apple Application Support"));
		logIt(_T("Apple Application Support=%s\n"), pAppleApplicationSupport);
		TCHAR* pAppleMobileDeviceSupport = get_apple_support_path(_T("Apple Mobile Device Support"));
		logIt(_T("Apple Mobile Device Support=%s\n"), pAppleMobileDeviceSupport);
		if (pAppleApplicationSupport != NULL)
		{
			_tcscpy_s(pp, nLen, _T(";"));
			_tcscpy_s(pp, nLen, pAppleApplicationSupport);
			SAFE_FREE(pAppleApplicationSupport);
		}
		if (pAppleMobileDeviceSupport != NULL)
		{
			_tcscpy_s(pp, nLen, _T(";"));
			_tcscpy_s(pp, nLen, pAppleMobileDeviceSupport);
			SAFE_FREE(pAppleMobileDeviceSupport);
		}
		_tputenv(pp);
		SAFE_FREE(pp);
	}
	free(p);
}
