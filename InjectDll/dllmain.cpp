// dllmain.cpp : Defines the entry point for the DLL application.
#include "stdafx.h"

#include <CoreFoundation/CFString.h>
#include <CoreFoundation/CFError.h>
#include <CoreFoundation/CFDictionary.h>
#include <CoreFoundation/CFNumber.h>
#include <CoreFoundation\CFPropertyList.h>
#include <detours.h>
#include <syelog.h>

void hook_AirTrafficHost_apis();

void logIt(TCHAR* fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	CString str;
	str.FormatV(fmt, args);
	OutputDebugString(str);
	va_end(args);

}

#define ENTER_FUNC_LOG()	logIt(_T("enter %S ++"),__FUNCTION__)

void logHex(const BYTE * buffer, int len, int bytesperline = 16)
{
	const BYTE * pBuffer = buffer;
	int buf_sz = len;
	TCHAR line_buf[75];
	size_t pos = 0;
	int line_count = 0;
	while (pos<buf_sz)
	{
		int line_pos_1 = 0;
		int line_pos_2 = 0;
		ZeroMemory(line_buf, sizeof(line_buf));
		_stprintf_s(line_buf, _T("%04d:"), line_count);
		line_buf[5] = ' ';
		line_pos_1 = 6;
		line_pos_2 = 54;
		for (size_t i = 0; i < bytesperline && pos<buf_sz; i++, pos++)
		{
			_stprintf_s(&line_buf[line_pos_1], 3, _T("%02x"), buffer[pos]);
			line_pos_1 += 2;
			line_buf[line_pos_1] = ' ';
			line_pos_1++;
			if (isprint(buffer[pos]))
				line_buf[line_pos_2] = (char)buffer[pos];
			else
				line_buf[line_pos_2] = '.';
			line_pos_2++;
		}
		for (; line_pos_1 < 54; line_pos_1++)
		{
			line_buf[line_pos_1] = ' ';
		}
		line_buf[line_pos_2] = 0;
		logIt(_T("%s\n"), line_buf);
		line_count++;
	}
}

typedef unsigned long long afc_file_ref;
typedef afc_file_ref	AFCFILEHANDLE;

FARPROC lpFunc = NULL;
FARPROC lpFuncSecureStartService = NULL;
FARPROC lpFuncStartService = NULL;
FARPROC lpFuncReceiveMessage = NULL;
FARPROC lpFuncSendMessage = NULL;
FARPROC lpFuncReceive = NULL;
FARPROC lpFuncSend = NULL;
FARPROC lpFuncFileRefOpen = NULL;
FARPROC lpFuncCopyValue = NULL;

//typedef int(__cdecl *AFCFileRefOpen)(HANDLE devHandle, const char *path, unsigned long long mode, AFCFILEHANDLE *fileHandle);

int hook_AFCFileRefOpen(HANDLE devHandle, const char *path, unsigned long long mode, AFCFILEHANDLE *fileHandle)
{
	logIt(_T("hook_AFCFileRefOpen++  mode=%d"), mode);
	logIt(_T("filename = %S"), path);
	int nRet = 1;
	if (lpFuncFileRefOpen != NULL)
	{
		nRet = ((int(__cdecl *)(HANDLE, const char *, unsigned long long, AFCFILEHANDLE *))lpFuncFileRefOpen)(devHandle, path, mode, fileHandle);
	}
	return nRet;
}

//typedef int(__cdecl *AMDServiceConnectionReceive)(HANDLE, char *buf, int size);
//typedef int(__cdecl *AMDServiceConnectionSend)(HANDLE, char *buf, int size);//it is my guess

int hook_AMDServiceConnectionReceive(HANDLE a, char *buf, int size)
{
	ENTER_FUNC_LOG();
	int nret = 1;
	if (lpFuncReceive != NULL)
	{
		nret = ((int(__cdecl *)(HANDLE, char *, int))lpFuncReceive)(a, buf, size);
		logHex((const BYTE *)buf, nret);
	}
	return nret;
}

int hook_AMDServiceConnectionSend(HANDLE a, char *buf, int size)
{
	ENTER_FUNC_LOG();
	logHex((const BYTE *)buf, size);
	int nret = 1;
	if (lpFuncSend != NULL)
	{
		nret = ((int(__cdecl *)(HANDLE, char *, int))lpFuncSend)(a, buf, size);
	}
	return nret;
}

//typedef int(__cdecl *AMDServiceConnectionReceiveMessage)(HANDLE, char *buf, int size, int);
//typedef int(__cdecl *AMDServiceConnectionSendMessage)(HANDLE, char *buf, int size, int);

int hook_AMDServiceConnectionSendMessage(HANDLE a, char *buf, int size, int b)
{
	ENTER_FUNC_LOG();
	CFShow(buf);

	int nret = 1;
	if (lpFuncSendMessage != NULL)
	{
		nret = ((int(__cdecl *)(HANDLE, char *, int, int))lpFuncSendMessage)(a, buf, size, b);
	}
	return nret;
}


int hook_AMDServiceConnectionReceiveMessage(HANDLE a, char *buf, int size, int b)
{
	ENTER_FUNC_LOG();
	int nret = 1;
	if (lpFuncReceiveMessage != NULL)
	{
		nret = ((int(__cdecl *)(HANDLE, char *, int, int))lpFuncReceiveMessage)(a, buf, size, b);
		CFShow(buf);
	}
	return nret;
}

int hook_AMDeviceSecureStartService(HANDLE a, CFStringRef service_name, int c, void* d)
{
	ENTER_FUNC_LOG();
	CFShow(service_name);
	int nret = 1;
	if (lpFuncSecureStartService != NULL)
	{
		nret = ((int(__cdecl *)(HANDLE, CFStringRef service_name, int, void*))lpFuncSecureStartService)(a, service_name, c, d);
	}
	return nret;
}

//typedef int(__cdecl *AMDeviceStartService)(HANDLE, CFStringRef service_name, HANDLE*, void* unkown);
int hook_AMDeviceStartService(HANDLE a, CFStringRef service_name, HANDLE* b, void* unkown)
{
	ENTER_FUNC_LOG();
	CFShow(service_name);
	int nret = 1;
	if (lpFuncStartService != NULL)
	{
		nret = ((int(__cdecl *)(HANDLE, CFStringRef service_name, HANDLE*, void* unkown))lpFuncStartService)(a, service_name, b, unkown);
	}
	return nret;
}


void* hook_AMRestorableDeviceRestore(void* device, CFMutableDictionaryRef options, void* callback, void* args)
{
	CFShow(options);
	void* ret = NULL;
	if (lpFunc != NULL)
	{
		ret = ((void* (__cdecl *)(void*, CFMutableDictionaryRef, void*, void*))lpFunc)(device, options, callback, args);
	}
	return ret;
}
//typedef CFStringRef(__cdecl *AMDeviceCopyValue)(HANDLE, CFStringRef, CFStringRef);
void * hook_AMDeviceCopyValue(HANDLE h, CFStringRef a, CFStringRef b)
{
	ENTER_FUNC_LOG();
	void * ret = NULL;
	if (a!=NULL)
	{
		logIt(_T("Domain="));
		CFShow(a);
	}
	CFShow(b);
	if (lpFuncCopyValue != NULL)
	{
		ret = ((void* (__cdecl *)(HANDLE, CFStringRef, CFStringRef))lpFuncCopyValue)(h, a, b);
		logIt(_T("return = "));
		CFShow(ret);
	}
	return ret;
}

void hook_iTunesMobileDevice_apis(HMODULE hLib)
{
	ENTER_FUNC_LOG();
	HMODULE h = LoadLibrary(_T("iTunesMobileDevice.dll"));
	lpFunc = GetProcAddress(h, "AMRestorableDeviceRestore");
	lpFuncSecureStartService = GetProcAddress(h, "AMDeviceSecureStartService");
	lpFuncStartService = GetProcAddress(h, "AMDeviceStartService");

	lpFuncReceiveMessage = GetProcAddress(h, "AMDServiceConnectionReceiveMessage");
	lpFuncSendMessage = GetProcAddress(h, "AMDServiceConnectionSendMessage");

	lpFuncReceive = GetProcAddress(h, "AMDServiceConnectionReceive");
	lpFuncSend = GetProcAddress(h, "AMDServiceConnectionSend");

	lpFuncFileRefOpen = GetProcAddress(h, "AFCFileRefOpen");

	lpFuncCopyValue = GetProcAddress(h, "AMDeviceCopyValue");
	

	if (lpFunc != NULL && lpFuncSecureStartService != NULL && lpFuncStartService!=NULL)
	{
		logIt(_T("DetourTransactionBegin++"));
		DetourTransactionBegin();
		DetourUpdateThread(GetCurrentThread());
		LONG l = DetourAttach(&(PVOID&)lpFunc, hook_AMRestorableDeviceRestore);
		if (l != 0) {
			logIt(_T("can not attach!"));
		}
		DetourAttach(&(PVOID&)lpFuncStartService, hook_AMDeviceStartService);
		DetourAttach(&(PVOID&)lpFuncSecureStartService, hook_AMDeviceSecureStartService);
		DetourAttach(&(PVOID&)lpFuncReceiveMessage, hook_AMDServiceConnectionReceiveMessage);
		DetourAttach(&(PVOID&)lpFuncSendMessage, hook_AMDServiceConnectionSendMessage);

		DetourAttach(&(PVOID&)lpFuncReceive, hook_AMDServiceConnectionReceive);
		DetourAttach(&(PVOID&)lpFuncSend, hook_AMDServiceConnectionSend);

		DetourAttach(&(PVOID&)lpFuncFileRefOpen, hook_AFCFileRefOpen);
		DetourAttach(&(PVOID&)lpFuncCopyValue, hook_AMDeviceCopyValue);

		hook_AirTrafficHost_apis();

		DetourTransactionCommit();
	}

}

typedef PVOID                       ATH_CONNECTION;

typedef
ATH_CONNECTION
(__cdecl *ATHostConnectionCreateWithLibrary)(
CFStringRef LibraryID,
CFStringRef Udid,
CFStringRef ATHPath
);

FARPROC lpFuncCreateWithLibrary = NULL;

void logItCFDict(CFDictionaryRef message)
{
	CFDataRef data = CFPropertyListCreateXMLData(nullptr, message);
	logIt(_T("%S, %d"),CFDataGetBytePtr(data), CFDataGetLength(data));
}

ATH_CONNECTION hook_ATHostConnectionCreateWithLibrary(
	CFStringRef LibraryID,
	CFStringRef Udid,
	CFStringRef ATHPath)
{
	ENTER_FUNC_LOG();
	CFShow(LibraryID); CFShow(Udid); CFShow(ATHPath);
	if (lpFuncCreateWithLibrary != NULL)
	{
		return ((ATHostConnectionCreateWithLibrary)lpFuncCreateWithLibrary)(LibraryID, Udid, ATHPath);
	}
	return (ATH_CONNECTION)0;
}

typedef
int
(__cdecl * ATHostConnectionSendPowerAssertion)(
ATH_CONNECTION  Connection,
CFBooleanRef    Enabled
);

FARPROC lpFuncSendPowerAssertion = NULL;
int hook_ATHostConnectionSendPowerAssertion(ATH_CONNECTION  Connection,
	CFBooleanRef    Enabled)
{
	ENTER_FUNC_LOG();
	CFShow(Enabled);

	if (lpFuncSendPowerAssertion != NULL)
	{
		return ((ATHostConnectionSendPowerAssertion)lpFuncSendPowerAssertion)(Connection, Enabled);
	}
	return 1;
}

typedef
int
(__cdecl *ATHostConnectionSendSyncRequest)(
ATH_CONNECTION  Connection,
CFArrayRef      Dataclasses,
CFDictionaryRef DataclassAnchors,
CFDictionaryRef HostInfo
);
FARPROC lpFuncSendSyncRequest = NULL;

int hook_ATHostConnectionSendSyncRequest(ATH_CONNECTION  Connection,
	CFArrayRef      Dataclasses,
	CFDictionaryRef DataclassAnchors,
	CFDictionaryRef HostInfo
	)
{
	ENTER_FUNC_LOG();
	CFShow(Dataclasses); CFShow(DataclassAnchors); CFShow(HostInfo);
	if (lpFuncSendSyncRequest != NULL)
	{
		return ((ATHostConnectionSendSyncRequest)lpFuncSendSyncRequest)(Connection, Dataclasses, DataclassAnchors, HostInfo);
	}
	return 1;
}


typedef
int
(__cdecl* ATHostConnectionSendHostInfo)(
ATH_CONNECTION  Connection,
CFDictionaryRef HostInfo
);
FARPROC lpFuncSendHostInfo = NULL;

int hook_ATHostConnectionSendHostInfo(ATH_CONNECTION  Connection,
	CFDictionaryRef HostInfo
	)
{
	ENTER_FUNC_LOG();
	CFShow(HostInfo);
	logItCFDict(HostInfo);
	if (lpFuncSendHostInfo != NULL)
	{
		return ((ATHostConnectionSendHostInfo)lpFuncSendHostInfo)(Connection, HostInfo);
	}
	return 1;
}


typedef
CFPropertyListRef
(__cdecl *ATHostConnectionReadMessage)(
ATH_CONNECTION Connection
);

FARPROC lpFuncReadMessage = NULL;

CFPropertyListRef hook_ATHostConnectionReadMessage(ATH_CONNECTION  Connection)
{
	ENTER_FUNC_LOG();
	if (lpFuncReadMessage != NULL)
	{
		CFPropertyListRef a = ((ATHostConnectionReadMessage)lpFuncReadMessage)(Connection);
		CFShow(a);
		logItCFDict(CFDictionaryRef(a));
		return a;
	}
	return NULL;
}


typedef
int
(__cdecl *ATHostConnectionSendMetadataSyncFinished)(
ATH_CONNECTION  Connection,
PVOID           cfmudictParam1,
PVOID           cfmudictParam2
);

FARPROC lpFuncSendMetadataSyncFinished = NULL;

int hook_ATHostConnectionSendMetadataSyncFinished(ATH_CONNECTION  Connection, 
	PVOID           cfmudictParam1,
	PVOID           cfmudictParam2)
{
	ENTER_FUNC_LOG();
	CFShow(cfmudictParam1); CFShow(cfmudictParam2);
	if (lpFuncSendMetadataSyncFinished != NULL)
	{
		return  ((ATHostConnectionSendMetadataSyncFinished)lpFuncSendMetadataSyncFinished)(Connection, cfmudictParam1, cfmudictParam2);
	}
	return 1;
}


typedef
int
(__cdecl *ATHostConnectionSendAssetCompleted)(
ATH_CONNECTION  Connection,
PVOID           cfstrParam1,
PVOID           cfstrParam2,
PVOID           cfstrParam3
);

FARPROC lpFuncSendAssetCompleted = NULL;

int hook_ATHostConnectionSendAssetCompleted(ATH_CONNECTION  Connection,
	PVOID           cfstrParam1,
	PVOID           cfstrParam2,
	PVOID           cfstrParam3)
{
	ENTER_FUNC_LOG();
	CFShow(cfstrParam1); CFShow(cfstrParam2); CFShow(cfstrParam3);
	if (lpFuncSendAssetCompleted != NULL)
	{
		return  ((ATHostConnectionSendAssetCompleted)lpFuncSendAssetCompleted)(Connection, cfstrParam1, cfstrParam2, cfstrParam3);
	}
	return 1;
}


typedef
int
(__cdecl *ATHostConnectionSendFileBegin)(
ATH_CONNECTION  Connection,
PVOID           cfstrParam1,
PVOID           cfstrParam2,
UINT64          cfstrParam3,
UINT64          cfstrParam4
);

FARPROC lpFuncSendFileBegin = NULL;

int hook_ATHostConnectionSendFileBegin(ATH_CONNECTION  Connection,
	PVOID           cfstrParam1,
	PVOID           cfstrParam2,
	UINT64          cfstrParam3,
	UINT64          cfstrParam4)
{
	ENTER_FUNC_LOG();
	CFShow(cfstrParam1); CFShow(cfstrParam2);
	if (lpFuncSendFileBegin != NULL)
	{
		return  ((ATHostConnectionSendFileBegin)lpFuncSendFileBegin)(Connection, cfstrParam1, cfstrParam2, cfstrParam3, cfstrParam4);
	}
	return 1;
}


typedef
int
(__cdecl *ATHostConnectionSendFileProgress)(
ATH_CONNECTION  Connection,
PVOID           cfstrParam1,
PVOID           cfstrParam2,
double          nParam3,
double          nParam4
);


typedef
int
(__cdecl *ATHostConnectionSendMessage)(
ATH_CONNECTION  Connection,
PVOID           cfstrParam1
);

FARPROC lpFuncATSendMessage = NULL;

int hook_ATHostConnectionSendMessage(ATH_CONNECTION  Connection,
	PVOID           cfstrParam1)
{
	ENTER_FUNC_LOG();
	CFShow(cfstrParam1); 
	logItCFDict(CFDictionaryRef(cfstrParam1));
	if (lpFuncATSendMessage != NULL)
	{
		return  ((ATHostConnectionSendMessage)lpFuncATSendMessage)(Connection, cfstrParam1);
	}
	return 1;
}


typedef
CFStringRef
(__cdecl *ATCFMessageGetName)(
CFPropertyListRef Message
);




typedef
CFDictionaryRef
(__cdecl *ATCFMessageGetParam)(
CFPropertyListRef Message
);



void hook_AirTrafficHost_apis()
{
	logIt(_T("hook_AirTrafficHost_apis++"));
	HMODULE h = LoadLibrary(_T("AirTrafficHost.dll"));

	lpFuncCreateWithLibrary = GetProcAddress(h, "ATHostConnectionCreateWithLibrary");
	lpFuncSendPowerAssertion = GetProcAddress(h, "ATHostConnectionSendPowerAssertion");
	lpFuncSendSyncRequest = GetProcAddress(h, "ATHostConnectionSendSyncRequest");
	lpFuncSendHostInfo = GetProcAddress(h, "ATHostConnectionSendHostInfo");
	lpFuncReadMessage = GetProcAddress(h, "ATHostConnectionReadMessage");
	lpFuncSendMetadataSyncFinished = GetProcAddress(h, "ATHostConnectionSendMetadataSyncFinished");
	lpFuncSendAssetCompleted = GetProcAddress(h, "ATHostConnectionSendAssetCompleted");
	lpFuncSendFileBegin = GetProcAddress(h, "ATHostConnectionSendFileBegin");
	lpFuncATSendMessage = GetProcAddress(h, "ATHostConnectionSendMessage");


	//DetourTransactionBegin();
	//DetourUpdateThread(GetCurrentThread());
	DetourAttach(&(PVOID&)lpFuncCreateWithLibrary, hook_ATHostConnectionCreateWithLibrary);
	DetourAttach(&(PVOID&)lpFuncSendPowerAssertion, hook_ATHostConnectionSendPowerAssertion);
	DetourAttach(&(PVOID&)lpFuncSendSyncRequest, hook_ATHostConnectionSendSyncRequest);
	DetourAttach(&(PVOID&)lpFuncSendHostInfo, hook_ATHostConnectionSendHostInfo);
	DetourAttach(&(PVOID&)lpFuncATSendMessage, hook_ATHostConnectionSendMessage);

	DetourAttach(&(PVOID&)lpFuncReadMessage, hook_ATHostConnectionReadMessage);
	DetourAttach(&(PVOID&)lpFuncSendMetadataSyncFinished, hook_ATHostConnectionSendMetadataSyncFinished);

	DetourAttach(&(PVOID&)lpFuncSendAssetCompleted, hook_ATHostConnectionSendAssetCompleted);
	DetourAttach(&(PVOID&)lpFuncSendFileBegin, hook_ATHostConnectionSendFileBegin);
	//DetourTransactionCommit();

}

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{

	if (DetourIsHelperProcess()) {
		return TRUE;
	}

	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		DetourRestoreAfterWith();		
		hook_iTunesMobileDevice_apis(GetModuleHandle(_T("iTunes.dll")));
		//hook_AirTrafficHost_apis();
		break;
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}

